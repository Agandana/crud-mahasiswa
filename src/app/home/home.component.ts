import { Component, OnInit } from '@angular/core';
import { AppState } from '../app.service';
import { Title } from './title';
import { XLargeDirective } from './x-large';
import { HttpClient } from '@angular/common/http';
import PouchDB from 'pouchdb';

@Component({
  selector: 'home',
  providers: [
    Title
  ],
  styleUrls: [ './home.component.css' ],
  templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit {
  public dummyData: any[] = [];
  public dataSearch: any[] = [];
  public mahasiswa: any;
  public namaMahasiswa: any;
  public nik: any;
  public tglLahir: any;
  public indexGlobal: any;
  public search: any;
  public searchName: any;
  public docData: any;
  public editMode = false;
  public disable = false;

  constructor(
    public appState: AppState,
    public http: HttpClient
  ) {}

  public ngOnInit() {
    // Menggunakan CouchDb yang ada di Local
    this.http.get('http://localhost:5984/mahasiswa/_all_docs?include_docs=true')
        .subscribe((response: any) => {
          this.dummyData = response.rows;
          this.dataSearch = response.rows;
        });
  }

  public createMahasiswa() {
    if (this.editMode) {
      // Menggunakan CouchDb yang ada di Local
      // tslint:disable-next-line:max-line-length
      this.http.put('http://localhost:5984/mahasiswa/' + this.docData.doc._id, {_id: this.docData.doc._id, _rev: this.docData.doc._rev, nik: this.nik, name: this.namaMahasiswa, dob: this.tglLahir})
          .subscribe((response: any) => {
            this.dummyData = response.rows;
            this.dataSearch = response.rows;
            window.location.reload();
          });
    }
    if (!this.editMode) {
      // Menggunakan CouchDb yang ada di Local
      // tslint:disable-next-line:max-line-length
      this.http.post('http://localhost:5984/mahasiswa', {nik: this.nik, name: this.namaMahasiswa, dob: this.tglLahir})
          .subscribe((response: any) => {
            this.dummyData = response.rows;
            this.dataSearch = response.rows;
            window.location.reload();
          });
    }
  }

  public reset() {
    this.disable = false;
    this.editMode = false;
    this.nik = '';
    this.namaMahasiswa = '';
    this.tglLahir = '';
  }

  public searchMahasiswa() {
    this.dummyData = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.dataSearch.length; i++) {
      if ((this.dataSearch[i].doc.name).toLowerCase().includes(this.search.toLowerCase())) {
        this.dummyData.push(this.dataSearch[i]);
      }
    }
  }

  public updateMahasiswa(index: any, data: any) {
    this.disable = true;
    this.docData = data;
    this.indexGlobal = index;
    this.editMode = true;

    this.nik = this.dummyData[index].doc.nik;
    this.namaMahasiswa = this.dummyData[index].doc.name;
    this.tglLahir = this.dummyData[index].doc.dob;
  }

  public deleteMahasiswa(data: any) {
    data._deleted = true;
    // Menggunakan CouchDb yang ada di Local
    // tslint:disable-next-line:max-line-length
    this.http.delete('http://localhost:5984/mahasiswa/' + data.id + '?rev=' + data.doc._rev)
        .subscribe((response) => {
          window.location.reload();
        });
  }

  public _keyPress(event: any) {
    const pattern = /^0$|^((?!(0))[0-9]{0,9})$/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }
}
